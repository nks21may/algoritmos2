public class Pair<X,Y> {
	private X a;
	private Y b;
	
	public Pair(){
		a = null;
		b = null;
	}

	public Pair(X x, Y y) {
		a = x;
		b = y;
	}
	
	public X getA(){
		return a;
	}
	
	public Y getB(){
		return b;
	}

	public void setA(X e){
		a = e;
	}
	
	public void setB(Y e){
		b = e;
	}

	@SuppressWarnings("unchecked")
	public Pair<String,String> concatPair(Pair<String,String> a){
		if (a == null){
			return (Pair<String, String>) this;
		}
		
		Pair<String,String> res = new Pair<String,String>();

		if (a.getA() == null || this.getA() == null) {
			if (a.getA() == null && this.getA() == null)
				res.setA(null);
			else{
				if (a.getA() == null)
					res.setA((String) this.getA());
				else
					res.setA(a.getA());
			}
		}else
			res.setA((String) this.getA()+ a.getA());
		
		if (a.getB() == null || this.getB() == null) {
			if (a.getB() == null && this.getB() == null)
				res.setB(null);
			else{
				if (a.getB() == null)
					res.setB((String) this.getB());
				else
					res.setB(a.getB());
			}
		}else
			res.setB((String)this.getB()+a.getB());
		return res;
	}
	
	@Override
    public int hashCode(){
        int result = a.hashCode();
        result = 31 * result + b.hashCode();
        return result;
    }
	
	@Override
	public String toString() {
		return "("+this.getA().toString()+","+this.getB().toString()+")";
	}
	
	public Pair<X,Y> clone(){
		return new Pair<X,Y>(this.getA(),this.getB());
	}
}