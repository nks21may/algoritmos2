import java.util.function.BiFunction;
import java.util.List;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.ArrayList;

public class App{

	static char C = '-'; //caracter especial
	final static BiFunction<String,String,Integer> D = ((xz, xc) -> Levenshtein.computeLevenshteinDistance(xz,xc));
	static String CAD1 = "";
	static String CAD2 = "";
	
	// Valida si una cadena esta conformada por los elementos un alfabeto determinado
	public static boolean validCad(String s, HashMap<Character,Boolean> m){
		boolean res = true;
		for(int i = 0; i < s.length() && res; i++)
			res &= m.containsKey(s.charAt(i));
		return res;
	}
	
	public static void usage(){
		System.out.println("java App <option> <argument> <option> <argument> ...");
		System.out.println("	Basic usage: java App <String cadena 1> <String cadena 2>");
		System.out.println("		This mode does not restrict the alphabet and does not define the highlighted symbol. Default set");
		System.out.println("_-|-_-|-_-|-_-|-_-|-_-|-_-|-_-|-_-|-_-|-_-|-_-|-_");
		System.out.println("	Option list:");
		System.out.println("		-h //show this");
		System.out.println("		-x 'String' //set the String 1");
		System.out.println("		-y 'String' //set the String 2");
		System.out.println("		-a 'String' //set the alphabet");
		System.out.println("		-c 'char' //set the highlighted symbol");
		System.out.println("		-d //set dinamic progam algoritm... not implemented yet");
		System.out.println("		-f //set force brute algoritm... not implemented yet");
		System.out.println("_-|-_-|-_-|-_-|-_-|-_-|-_-|-_-|-_-|-_-|-_-|-_-|-_");
		System.out.println("Complete example: java App -c '_' -a 'ABCDE' -x 'ABBECD' -y 'CDA' ");
		
		System.exit(0);
	}
	
	//Analiza si las cadenas pasadas como parametro son validas
	public static void analizadorDeParametros(String[] args){ 																																																																							//necesito consejos de como hacer esto mejor. Si tiene alguno, por favor envielo junto con la devolucion. Escondi el comentario por que me lo borran. Happy Easter Egg.
		if(args.length < 2)
			usage();
		if (args.length == 2) {
			CAD1 = args[0];	
			CAD2 = args[1];	
		}else{
			int i = 0;
			HashMap<Character,Boolean> alphabet = new HashMap<Character,Boolean>(); 	
			while(i < args.length){
				if (args[i].charAt(0) == '-' &&  1 < args[i].length() && args.length > i+1) {
					switch (Character.toLowerCase(args[i].charAt(1))) {
					case 'x':
						i++;
						if(CAD1 == "")
							CAD1 = args[i].toUpperCase();
						else
							usage();
						break;
					case 'y':
						i++;
						if(CAD2 == "")
							CAD2 = args[i].toUpperCase();
						else
							usage();
						break;
					case 'a':
						i++;
						if (alphabet.isEmpty()) {
							for(char c: args[i].toUpperCase().toCharArray())
								alphabet.put(new Character(c),true);
						}else{
							usage();
						}
						break;
					case 'c':
						i++;
						C = args[i].charAt(0);
						break;
					default:
						usage();
						break;
					}
				}else{
					usage();
				}
				i++;
			}
			if (CAD1 != "" && CAD2 != "" && !alphabet.isEmpty()) {
				if (!validCad(CAD1, alphabet) || !validCad(CAD2, alphabet))
					throw new IllegalArgumentException("Las cadenas contienen Caracteres no pertenecientes al alfabeto");
			}else{
				throw new IllegalArgumentException("Mal uso de parametros");
			}
		}
	}
	
	//Genera la lista de posibles alineaciones entre dos palabras
	public static List<Pair<String, String>> genAlineationForceBrute2(String x, String y){

		LinkedList<Object[]> act = new LinkedList<Object[]>();
		act.add(new Object[]{"","",0,0});
		ArrayList<Pair<String, String>> res = new ArrayList<Pair<String,String>>();
		int maxl = x.length()+ y.length();

		//generador
		while(!act.isEmpty()){
			Object[] elem = act.poll();
			String cad1 = (String) elem[0];
			String cad2 = (String) elem[1];
			Integer i1 = (Integer) elem[2];
			Integer i2 = (Integer) elem[3];
			if (i1 < x.length() && i2 < y.length()){
				act.add(new Object[]{cad1+x.charAt(i1), cad2+y.charAt(i2), new Integer(i1+1),new Integer(i2+1)});// caso "A" "A"
				act.add(new Object[]{cad1+C, cad2+y.charAt(i2), new Integer(i1), new Integer(i2+1)}); //caso "-" "A"
				act.add(new Object[]{cad1+x.charAt(i1), cad2+C, new Integer(i1+1),new Integer(i2)});// caso "A" "-"
			}else{
				if (i1 < x.length() && cad1.length() < maxl) {			
					act.add(new Object[]{cad1+x.charAt(i1), cad2, new Integer(i1+1),new Integer(i2)}); // "A" ""  
					if(i1 < i2){
						if(cad2.charAt(i1) != C) // me fijo si abajo no tiene un gion
							act.add(new Object[]{cad1+C, cad2, new Integer(i1),new Integer(i2)}); // "-" "" 
					}
				}else{ 
					if (i2 < y.length() && cad2.length() < maxl) {
						act.add(new Object[]{cad1, cad2+y.charAt(i2), new Integer(i1),new Integer(i2+1)}); // "" "A"
						if(i2 < i1){
							if(cad1.charAt(i2)!= C) // me fijo si abajo no tiene un gion
								act.add(new Object[]{cad1, cad2+C, new Integer(i1),new Integer(i2)}); // "" "-"
						}
					}else{	
						if(i1 == x.length() && i2 == y.length()) //me aseguro que consuma todos los caracteres de la cadena original
							res.add(new Pair<String,String>(cad1, cad2)); //caso que ya sea una hoja
					}
				}
			}
		}
		return res;
	}

	//Retorna una lista que contiene la alineacion optima cuyo valor retornado por d es el minimo y igual al resto de los elementos de la misma.
	public static List<Pair<String,String>> bestAlineation(List<Pair<String,String>> lista, BiFunction<String, String, Integer> d){
		int min = Integer.MAX_VALUE;
		ArrayList<Pair<String,String>> res = new ArrayList<Pair<String,String>>();
		for (Pair<String, String> par : lista) {
			int r = d.apply(par.getA(), par.getB());
			if (r < min) {
				res.clear();
				min = r;
			}
			if (r == min) {
				res.add(par);
			}
		}
		return res;
	}

	//Retorna el numero de coincidencias entre las secuencias alineadas
	public static int coinci(Pair<String,String> par){
		int c=0;
		int i=0;
		String a = par.getA();
		String b = par.getB();
		int mn = Math.min(a.length(),b.length());
		while(i<mn){
			if(a.charAt(i)==b.charAt(i))
				c++;
			i++;
		}
		return c;
	}

	//Retorna el par que contiene mayor cantidad de coincidencias entre sus aliaciones
	public static Pair<String,String> bestCoinc(List<Pair<String,String>> lis){
		int i=lis.size();
		int j=1;
		Pair<String,String> res = lis.get(0);
		while(j<i){
			if(coinci(res)<coinci(lis.get(j)))
				res = lis.get(j);
			j++;
		}
		return res;
	}

	public static void main(String[] args){
		analizadorDeParametros(args);
		List<Pair<String,String>> x = bestAlineation(genAlineationForceBrute2(CAD1,CAD2), D);
		Pair<String,String> pair = bestCoinc(x);
		System.out.println("La alineacion entre las secuencias ["+CAD1+"] y ["+CAD2+"] es:" );
		System.out.println(pair+": costo de edicion -> "+ D.apply(pair.getA(), pair.getB()));
		System.out.println();
		System.out.println("Otras posibles soluciones con el mismo costo son:");
		for (Pair<String, String> par : x) {
			if (!x.equals(par) && par != null) {
				System.out.println(par);
			}
		}
	}
}