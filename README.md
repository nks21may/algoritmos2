﻿•Proyecto N° 1 | Algoritmos 2-Diseño de Algoritmos
        AÑO:2018
        
•Integrantes:
    -D'alessandro, Nicolas
        DNI: 41.105.833
    -García, Valeria
        DNI: 39.545.287

•Notas Importantes:
	* para cambiar la funcion de costos "D", modifique la constante global D reemplazando Levenshtein.computeLevenshteinDistance por su funcion.
	* Si desea usar las opciones debe colocar como minimo -x "Cadena", -y "Cadena", -a "alfabeto" en el orden que quiera
	* El uso basico (con las 2 cadenas a alinear) considera que toda la tabla ascii es el abecedario. Por lo tanto no lo comprueba. 

•Syntax: java App <option> <argument> <option> <argument> ...
		Basic usage: java App <String cadena 1> <String cadena 2>
			This mode does not restrict the alphabet and does not define the highlighted symbol. Default set
	
		Option list:
			-h //show this
			-x 'String' //set the String 1
			-y 'String' //set the String 2
			-a 'String' //set the alphabet
			-c 'char' //set the highlighted symbol
	Complete example: java App -c "-" -a 'ABCDE' -x 'ABBECD' -y 'CDA' 

•Repositorio en BitBucket: https://bitbucket.org/nks21may/algoritmos2/src/master/